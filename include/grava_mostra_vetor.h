/**
 * @file	grava_mostra_vetor.h
 * @brief	Implementacao das funcoes que carregam e printam o vetor
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	04/04/2017
 */

#ifndef GRAVA_MOSTRA_H
#define GRAVA_MOSTRA_H

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cstring>

using std::cout;
using std::cin;
using std::endl;
using std::string;

/** 
 * @brief	Funcao generica que carrega os elementos do vetor randomicamente
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 */
template < typename Ler>
void preenchimento_vetor (Ler* vetor, int tamanho)
{
    srand (time (NULL));

    int ii, jj;
    for (ii=0; ii<tamanho; ii++)
    {
        vetor [ii] = rand () % tamanho + 1;
        //1 + (Ler)rand()/((Ler)RAND_MAX/(Ler)(tamanho-1));

        for(jj=0; jj<ii; jj++)
        {
            if(vetor[jj] == vetor[ii])
            {
                vetor[ii] = rand () % tamanho + 1;
                jj = -1;
            }
        }
    }
}

/** 
 * @brief	Funcao especializada para preencher os elementos do vetor com strings fornecidos pelo usuario
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 */
template <>
void preenchimento_vetor <string> (string* vetor, int tamanho)
{
    cout << endl << "Digite as strings:" << endl;

    int ii;
    for (ii=0; ii<tamanho; ii++)
    {
        cout << "String " << ii+1 << ": ";
        cin >> vetor[ii];
    }

    cout << endl;
}

/** 
 * @brief	Funcao generica que imprime os elementos de um vetor
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 */
template < typename Mostra>
void printar_vetor (Mostra* vetor, int tamanho)
{
    int ii;

    cout << "Vetor: [";
    for (ii=0; ii<tamanho; ii++)
    {
        if (ii != tamanho-1)
            cout << vetor[ii] << ", ";
        else
            cout << vetor[ii] << "]" << endl << endl;
    }
}

/** 
 * @brief	Funcao especializada que imprime os elementos de um vetor de strings
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 */
template <>
void printar_vetor <string> (string* vetor, int tamanho)
{
    int ii;

    cout << "Vetor: [";
    for (ii=0; ii<tamanho; ii++)
    {
        if (ii != tamanho-1)
            cout << vetor[ii] << ", ";
        else
            cout << vetor[ii] << "]" << endl << endl;
    }
}

#endif