/**
 * @file	funcs_menu.h
 * @brief	Declaracao dos prototipos das funcoes usadas no menu para leitura e interação com o usuario
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	01/04/2017
 */

#ifndef FUNCS_MENU_H
#define FUNCS_MENU_H

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <cstring>
using std::string;

/** 
 * @brief	Funcao que faz a leitura do tipo de dados e tamanho para um vetor
 * @param 	tipo_dados Tipo do vetor
 * @param	tamanho Tamanho do vetor
 */
void leitura_inicial (string &tipo_dados, int* tamanho);

/** 
 * @brief	Funcao que mostra ao usuario as opcoes do menu e o usuario escolhe qual opcao deseja
 * @param   vint Vetor de inteiros
 * @param   vfloat Vetor de float
 * @param   vdouble Vetor de double
 * @param   vstrins Vetor de strings
 * @param 	tipo_dados Tipo do vetor
 * @param	tamanho Tamanho do vetor
 */
void menu (int* vint, float* vfloat, double* vdouble, string* vstring, string tipo_dados, int tamanho);

#endif