/**
 * @file	calc_operacoes.h
 * @brief	Implementacao das funcoes que chamam as funcoes de determinadas operacoes e imprime os resultados
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	04/04/2017
 */

#ifndef CALC_OPERACOES_H
#define CALC_OPERACOES_H

#include "operacoes.h"

/** 
 * @brief 	Enum com as operacoes que podem ser realizadas
 * @details A enumeracao comeca com 1 e as outras é incrementado um a um
 */
typedef enum Operacoes
{
    opMax = 1,
    opMin,
    opSum,
    opAvg,
    opHig,
    opLow
} tipo_operacao;


/** 
 * @brief	Funcao generica que faz a chamada para a funcao da operacao desejada em um vetor numerico
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   operacao Operacao desejada pelo usuario
 * @param   valor Valor que sera usado como base ou limite em algumas funcoes
 * @return  Resultado da operacao desejada
 */
template <typename Tipo>
Tipo doVector (Tipo* vetor, int tamanho, tipo_operacao operacao, Tipo valor = 0)
{
    Tipo (*ptr_func) (Tipo*, int, Tipo);

    switch (operacao)
    {
        case opMax:
            ptr_func = maximo;
        break;

        case opMin:
            ptr_func = minimo;
        break;

        case opSum:
            ptr_func = soma;
        break;

        case opAvg:
            ptr_func = media;
        break;

        case opHig:
            ptr_func = maior_valor;
        break;

        case opLow:
            ptr_func = menor_valor;
        break;
    }

    return ptr_func (vetor, tamanho, valor);
}

/** 
 * @brief	Funcao especializada que faz a chamada para a funcao da operacao desejada em um vetor de strings
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   operacao Operacao desejada pelo usuario
 * @param   valor Valor que sera usado como base ou limite em algumas funcoes
 * @return  Resultado da operacao desejada
 */
template <>
string doVector <string> (string* vetor, int tamanho, tipo_operacao operacao, string valor)
{
    string (*ptr_func) (string*, int, string);

    switch (operacao)
    {
        case opMax:
            ptr_func = maximo;
        break;

        case opMin:
            ptr_func = minimo;
        break;

        case opSum:
            ptr_func = soma;
        break;

        case opAvg:
            ptr_func = media;
        break;

        case opHig:
            ptr_func = maior_valor;
        break;

        case opLow:
            ptr_func = menor_valor;
        break;
    }

    return ptr_func (vetor, tamanho, valor);
}

/** 
 * @brief	Funcao generica que imprime o resultado da operacao desejada em um vetor numerico 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   operacao Operacao desejada pelo usuario
 */
template < typename Tipo0 >
void resultados (Tipo0* vetor, int tamanho, tipo_operacao operacao)
{
    Tipo0 valor;        // Usado como base ou limite, dependendo da funcao

    switch (operacao)
    {
        case opMax:
            cout << "Maior valor do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao)) << endl << endl;
        break;

        case opMin:
            cout << "Menor valor do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao)) << endl << endl;
        break;

        case opSum:
            cout << "Soma dos elementos do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao)) << endl << endl;
        break;

        case opAvg:
            cout << "Media dos elementos do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao)) << endl << endl;
        break;

        case opHig:
            cout << "Informe o valor base: ";
            cin >> valor;

            cout << "Quantidades de elemento maiores que " << valor << ": ";
            cout << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opLow:
            cout << "Informe o valor limite: ";
            cin >> valor;

            cout << "Quantidades de elemento menores que " << valor << ": ";
            cout << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;
    }
}

/** 
 * @brief	Funcao especializada que imprime o resultado da operacao desejada em um vetor de strings 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   operacao Operacao desejada pelo usuario
 */
template <>
void resultados <string> (string* vetor, int tamanho, tipo_operacao operacao)
{
    string valor = " ";     // Usado como base ou limite, dependendo da funcao

    switch (operacao)
    {
        case opMax:
            cout << "Maior string do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opMin:
            cout << "Menor string do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opSum:
            cout << "Concatenação das strings do vetor: " << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opAvg:
            cout << "Concatenação das strings do vetor cujo tamanho são iguais a media: " << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opHig:
            cout << "Informe o valor base: ";
            cin >> valor;

            cout << "Concatenação das strings maiores que " << valor << ": ";
            cout << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;

        case opLow:
            cout << "Informe o valor limite: ";
            cin >> valor;

            cout << "Concatenação das strings menores que " << valor << ": ";
            cout << doVector (vetor, tamanho, tipo_operacao(operacao), valor) << endl << endl;
        break;
    }
}

#endif
