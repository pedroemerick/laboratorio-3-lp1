/**
 * @file	operacoes.h
 * @brief	Implementacao das funcoes que realizam determinadas operacoes em vetores
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	04/04/2017
 */

#ifndef OPERACOES_H
#define OPERACOES_H

#include <iostream>

#include <cstring>
using std::string;

#include <sstream>
using std::stringstream;

#include <cstdlib>

/** 
 * @brief	Funcao generica que retorna o maior valor presente em um vetor 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor Número nao utilizado nesta função que tem por padrao valor 0
 * @return  Maior valor do vetor
 */
template < typename Tipo1 >
Tipo1 maximo (Tipo1* vetor, int tamanho, Tipo1 valor = 0)
{
    Tipo1 maior = vetor [0];
    int ii;

    for (ii=1; ii<tamanho; ii++)
    {
        if (vetor[ii] > maior)
            maior = vetor [ii];
    }

    return maior;
}

/** 
 * @brief	Funcao especializada que retorna a string de maior tamanho presente em um vetor de strings 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor String nao utilizada nesta função que tem por padrao valor " "
 * @return  String de maior tamanho do vetor
 */
template <>
string maximo <string> (string* vetor, int tamanho, string valor)
{
    string maior = vetor [0];
    int ii;

    for (ii=1; ii<tamanho; ii++)
    {
        if (vetor[ii].size () > maior.size ())
            maior = vetor [ii];
    }

    return maior;
}

/** 
 * @brief	Funcao generica que retorna o menor valor presente em um vetor 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor Número nao utilizado nesta função que tem por padrao valor 0
 * @return  Menor valor do vetor
 */
template < typename Tipo2 >
Tipo2 minimo (Tipo2* vetor, int tamanho, Tipo2 valor = 0)
{
    Tipo2 menor = vetor [0];
    int ii;

    for (ii=1; ii<tamanho; ii++)
    {
        if (vetor[ii] < menor)
            menor = vetor [ii];
    }

    return menor;
}

/** 
 * @brief	Funcao especializada que retorna a string de menor tamanho presente em um vetor de strings
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor String nao utilizada nesta função que tem por padrao valor " "
 * @return  String de menor tamanho do vetor
 */
template <>
string minimo <string> (string* vetor, int tamanho, string valor)
{
    string menor = vetor [0];
    int ii;

    for (ii=1; ii<tamanho; ii++)
    {
        if (vetor[ii].size () < menor.size ())
            menor = vetor [ii];
    }

    return menor;
}

/** 
 * @brief	Funcao generica que retorna a soma de todos os valores presentes em um vetor 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor Número nao utilizado nesta função que tem por padrao valor 0
 * @return  Soma dos valores do vetor
 */
template < typename Tipo3 >
Tipo3 soma (Tipo3* vetor, int tamanho, Tipo3 valor = 0)
{
    Tipo3 soma = 0;
    int ii;

    for (ii=0; ii<tamanho; ii++)
    {
        soma += vetor [ii];
    }

    return soma;
}

/** 
 * @brief	Funcao especializada que retorna a concatenação de todas as strings contidas no vetor
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor String nao utilizada nesta função que tem por padrao valor " "
 * @return  Concatenação de todas as strings contidas no vetor
 */
template <>
string soma <string> (string* vetor, int tamanho, string valor)
{
    string soma = vetor[0];
    int ii;

    for (ii=1; ii<tamanho; ii++)
    {
        soma = soma + vetor[ii];
    }

    return soma;
}

/** 
 * @brief	Funcao generica que retorna o valor médio (parte inteira) de todos os valores presentes em um vetor 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor Número nao utilizado nesta função que tem por padrao valor 0
 * @return  Valor médio de todos os valores presentes no vetor
 */
template < typename Tipo4 >
Tipo4 media (Tipo4* vetor, int tamanho, Tipo4 valor = 0)
{
    int media_int = Tipo4 (valor);

    media_int = (soma (vetor, tamanho)) / tamanho;

    return media_int;
}

/** 
 * @brief	Funcao especializada que retorna a concatenação das strings cujo tamanho seja igual à média dos tamanhos de todas
            as strings contidas no vetor. Caso tais strings não existam, a função retorna uma string vazia.
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   valor String nao utilizada nesta função que tem por padrao valor " "
 * @return  Concatenacao das strings cujo tamanho seja igual a media ou string vazia
 */
template <>
string media <string> (string* vetor, int tamanho, string valor)
{
    unsigned int media = 0;
    string media_s = "";
    int ii;

    for (ii=0; ii<tamanho; ii++)
    {
        media += vetor[ii].size ();
    }

    media = media / tamanho;

    int cont = 0;

    for (ii=0; ii<tamanho; ii++)
    {
        if (vetor[ii].size () == media)
            media_s += vetor[ii];
        else 
            cont++;
    }

    if (cont == tamanho)
        media_s = "";

    return media_s;
}

/** 
 * @brief	Funcao generica que retorna a quantidade de elementos em um vetor que são maiores que um determinado valor
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   base Valor base que o usuario deseja
 * @return  Quantidade de elementos maiores que o valor base
 */
template < typename Tipo5 >
Tipo5 maior_valor (Tipo5* vetor, int tamanho, Tipo5 base)
{
    int qntd = 0;
    int ii;

    for (ii=0; ii<tamanho; ii++)
    {
        if (vetor[ii] > base)
            qntd ++;
    }

    return qntd;
}

/** 
 * @brief	Funcao especializada que retorna a concatenação das strings cujo tamanho é maior que um determinado valor base 
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   base Valor base que o usuario deseja
 * @return  Concatenação das strings cujo tamanho é maior que o valor base
 */
template <>
string maior_valor <string> (string* vetor, int tamanho, string base)
{
    string qntd;
    int ii;

    unsigned int base_i;
    base_i = atoi (base.c_str());

    for (ii=0; ii<tamanho; ii++)
    {
        if (vetor[ii].size () > base_i)
            qntd += vetor[ii];
    }
    
    return qntd;
}

/** 
 * @brief	Funcao generica que retorna a quantidade de elementos em um vetor que são menores que um determinado valor
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   limite Valor limite que o usuario deseja
 * @return  Quantidade de elementos menores que o valor limite
 */
template < typename Tipo6 >
Tipo6 menor_valor (Tipo6* vetor, int tamanho, Tipo6 limite)
{
    int qntd = 0;
    int ii;

    for (ii=0; ii<tamanho; ii++)
    {
        if (vetor[ii] < limite)
            qntd ++;
    }

    return qntd;
}

/** 
 * @brief	Funcao especializada que retorna a concatenação das strings cujo tamanho é menor que um determinado valor limite
 * @param 	vetor Vetor usado
 * @param	tamanho Tamanho do vetor
 * @param   limite Valor limite que o usuario deseja
 * @return  Concatenação das strings cujo tamanho é menor que um determinado valor limite
 */
template <>
string menor_valor <string> (string* vetor, int tamanho, string limite)
{
    string qntd;
    int ii;

    unsigned int limite_i;
    limite_i = atoi (limite.c_str());

    for (ii=0; ii<tamanho; ii++)
    {
        if (vetor[ii].size () < limite_i)
            qntd += vetor[ii];
    }

    return qntd;
}

#endif
