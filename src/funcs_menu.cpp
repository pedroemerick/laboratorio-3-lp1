/**
 * @file	funcs_menu.cpp
 * @brief	Implementacao das funcoes usadas no menu para leitura e interação com o usuario
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	04/04/2017
 */

#include "funcs_menu.h"
#include "calc_operacoes.h"

/** 
 * @brief	Funcao que faz a leitura do tipo de dados e tamanho para um vetor
 * @param 	tipo_dados Tipo do vetor
 * @param	tamanho Tamanho do vetor
 */
void leitura_inicial (string &tipo_dados, int* tamanho)
{   
    int dados = 1;

    while (dados != 0)
    {
        cout << "Tipo de dados: ";
        cin >> tipo_dados;

        if (tipo_dados != "float" && tipo_dados != "int" && tipo_dados != "double" && tipo_dados != "string" )
            cout << "ERRO !!! Tipo invalido. Tente novamente !!!" << endl << endl;
        else
        {
            cout << "Informe a quantidade de elementos: ";
            cin >> *tamanho;

            if (*tamanho <= 0)
                cout << "ERRO !!! Tamanho invalido. Tente novamente !!!" << endl << endl;
            else 
                dados = 0;
        }
    }
}

/** 
 * @brief	Funcao que mostra ao usuario as opcoes do menu e o usuario escolhe qual opcao deseja
 * @param   vint Vetor de inteiros
 * @param   vfloat Vetor de float
 * @param   vdouble Vetor de double
 * @param   vstrins Vetor de strings
 * @param 	tipo_dados Tipo do vetor
 * @param	tamanho Tamanho do vetor
 */
void menu (int* vint, float* vfloat, double* vdouble, string* vstring, string tipo_dados, int tamanho)
{
    int sair = 1;

    while (sair != 0)
    {
        cout << "Operacoes:" << endl << endl;
        cout << "(1) Maior valor" << endl;
        cout << "(2) Menor valor" << endl;
        cout << "(3) Soma dos elementos" << endl;
        cout << "(4) Media dos elementos" << endl;
        cout << "(5) Elementos maiores que um valor" << endl;
        cout << "(6) Elementos menores que um valor" << endl;
        cout << "(0) Sair" << endl << endl;
        cout << "Digite sua opcao: ";

        int operacao = 0;    // Opcao de operacao a ser escolhida pelo usuario
        cin >> operacao;   
        
        if (operacao < 0 || operacao > 6)
            cout << "ERRO !!! Opcao invalida. Tente novamente !!!" << endl << endl;

        else if (operacao != 0) {
            if (tipo_dados == "int") {
                resultados (vint, tamanho, tipo_operacao(operacao));
            } else if (tipo_dados == "float") {
                resultados (vfloat, tamanho, tipo_operacao(operacao));
            } else if (tipo_dados == "string") {
                resultados (vstring, tamanho, tipo_operacao(operacao));
            } else {
                resultados (vdouble, tamanho, tipo_operacao(operacao));
            }
        }
        else 
            sair = 0;
    }

	cout << endl << "Programa encerrado." << endl;
}