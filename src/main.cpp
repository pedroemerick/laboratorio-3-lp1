/**
 * @file	main.cpp
 * @brief	Arquivo principal
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/03/2017
 * @date	01/04/2017
 */

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <cstring>
using std::string;

#include "grava_mostra_vetor.h"
#include "funcs_menu.h"

/** @brief Funcao principal */
int main ()
{
    string tipo_dados;				// Tipo de dados (int, float, double, string)
    int tamanho;                    // Tamanho do vetor

    leitura_inicial (tipo_dados, &tamanho);

	// Possiveis vetores a serem alocados dinamicamente conforme a necessidade
	int* vint = NULL;			// Vetor de inteiros
	float* vfloat = NULL;		// Vetor de decimais (float)
	double* vdouble = NULL;		// Vetor de decimais (double)
	string* vstring = NULL;		// Vetor de strings

    // Alocacao, preenchimento e impressao do vetor de acordo com o tipo
	if (tipo_dados == "int") {

        vint = new int [tamanho];

        preenchimento_vetor (vint, tamanho);

        printar_vetor (vint, tamanho);
	} 
    else if (tipo_dados == "float") {

        vfloat = new float [tamanho];

        preenchimento_vetor (vfloat, tamanho);

        printar_vetor (vfloat, tamanho);
    }
    else if (tipo_dados == "double") {

        vdouble = new double [tamanho];

        preenchimento_vetor (vdouble, tamanho);

        printar_vetor (vdouble, tamanho);
    }
    else if (tipo_dados == "string") {

        vstring = new string [tamanho];

        preenchimento_vetor (vstring, tamanho);

        printar_vetor (vstring, tamanho);
    }

    menu (vint, vfloat, vdouble, vstring, tipo_dados, tamanho);

    // Liberando vetores alocados dinamicamente
    delete [] vint;
    delete [] vfloat;
    delete [] vdouble;
    delete [] vstring;

    return 0;
}